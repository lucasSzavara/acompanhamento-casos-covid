# Acompanhamento de casos de COVID-19

O projeto usa Python 3.8, que pode ser instalado [aqui](https://www.python.org/downloads/release/python-3810/).

Após a instalação do Python, crie e ative o ambiente virtual usando o python 3.8, seja pelo
[conda](https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html),
[virtual env](https://docs.python.org/pt-br/3/library/venv.html) ou pelo seu gerenciador de ambientes preferido.
Em seguida instale os pacotes necessários para execução:

```bash
pip install -r requirements.txt
```

E rode o projeto:

```bash
gunicorn index:server
```

A aplicação demora alguns minutos para abrir pela primeira vez após rodar o comando. Por padrão a aplicação estará
disponivel em [0.0.0.0:8000](0.0.0.0:8000). Atualmente a aplicação também está disponível 
[aqui](https://practical-theme-320921.ue.r.appspot.com), embora ela possa demorar alguns minutos para executar se a
aplicação não tiver recebido requisições nos últimos minutos.
